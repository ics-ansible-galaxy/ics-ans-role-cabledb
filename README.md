# ics-ans-role-cabledb

Ansible role to install Cable DB.

## Requirements

- ansible >= 2.7
- molecule >= 2.19

## Role Variables

```yaml
cabledb_network: cabledb-network
cabledb_container_name: cabledb
cabledb_container_image: "{{ cabledb_container_image_name }}:{{ cabledb_container_image_tag }}"
cabledb_container_image_name: registry.esss.lu.se/ics-software/cable-db
cabledb_container_image_tag: latest
cabledb_container_image_pull: "True"
cabledb_env: {}
cabledb_frontend_rule: "Host:{{ ansible_fqdn }}"

# To use external database host, define 'cabledb_database_host'
# cabledb_database_host: database-host
cabledb_database_container_name: cabledb-database
cabledb_database_image: postgres:9.6.7
cabledb_database_published_ports: []
cabledb_database_volume: cabledb-database
cabledb_database_name: cabledb_dev
cabledb_database_username: cabledb_dev
cabledb_database_password: cabledb_dev
```

## Example Playbook

```yaml
- hosts: servers
  roles:
    - role: ics-ans-role-cabledb
```

## License

BSD 2-clause
